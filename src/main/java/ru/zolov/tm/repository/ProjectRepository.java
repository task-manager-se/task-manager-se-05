package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projects = new LinkedHashMap<>();

    public ProjectRepository() {
    }

    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    public Project findOne(String id) {
        return projects.get(id);
    }

    public List<Project> findAll() {
        List<Project> projectsList = new ArrayList<>(projects.values());
        return projectsList;
    }

    public void update(String id, String name, String description, Date start, Date finish) {
        Project project = projects.get(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
    }

    public boolean remove(String id) {
        return projects.remove(id) != null;
    }

    public void removeAll() {
        projects.clear();
    }

    public void merge(String id, String name, String description, Date start, Date finish) {
        if (findOne(id) == null) {
            persist(new Project(name, description, start, finish));
        } else {
            update(id, name, description, start, finish);
        }
    }

}

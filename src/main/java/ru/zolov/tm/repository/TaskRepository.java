package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public void persist(String id) {
        Task task = new Task(id);
        tasks.put(task.getId(), task);

    }

    public void persist(String id, String name) {
        Task task = new Task(id);
        task.setName(name);
        tasks.put(task.getId(), task);
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public List<Task> findAll() {
        List<Task> taskList = new ArrayList<>(tasks.values());
        return taskList;
    }

    public List<Task> findTaskByProjId(String id) {
        List<Task> result = new ArrayList<>();

        for (Task task : tasks.values()) {
            if (task.getProjectId().equals(id)) {
                result.add(task);
            }
        }
        return result;
    }

    public void update(String id, String description) {
        Task task = tasks.get(id);
        task.setName(description);
    }

    public boolean remove(String id) {
        return tasks.remove(id) != null;
    }

    public void removeAll() {
        tasks.clear();
    }

    public boolean removeAllByProjectID(String id) {
        for (Task task : tasks.values()) {
            if (task.getProjectId().equals(id)) {
                tasks.remove(task.getId());
                return true;
            }
        }
        return false;
    }

    public void merge(String id, String name) {
        if (findOne(id) == null) {
            persist(name);
        } else {
            update(id, name);
        }
    }
}



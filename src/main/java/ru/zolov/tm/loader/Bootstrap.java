package ru.zolov.tm.loader;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private final TerminalService terminalService = new TerminalService();
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    public void registry(AbstractCommand command) throws Exception {
        String cliCommand = command.getName();
        String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception("Wrong command!");
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception("Wrong description!");
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void registry(Class... classes) throws Exception {
        for (Class clazz : classes) registry(clazz);
    }

    public void registry(Class clazz) throws IllegalAccessException, InstantiationException, Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        Object command = clazz.newInstance();
        AbstractCommand abstractCommand = (AbstractCommand) command;
        registry(abstractCommand);
    }

    public void init(Class... classes) throws Exception {
        if (classes == null || classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        start();
    }

    public void start() throws Exception {
        System.out.println("+-------------------------------+");
        System.out.println("|    Welcome to Task manager    |");
        System.out.println("+-------------------------------+");

        String command;

        do {
            command = terminalService.nextLine();
            execute(command);
        } while (!command.equals("exit"));
    }

    public void execute(String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("abstractCommand == null");
            return;
        }
        abstractCommand.execute();
    }
}


package ru.zolov.tm.service;

import ru.zolov.tm.entity.Project;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void create(String name, String description, Date start, Date finish) throws Exception {

        if (name == null || name.isEmpty()) throw new Exception("Name input empty");
        Project project = new Project(name);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
        if (description == null || description.isEmpty()) project.setDescription("empty");
        projectRepository.persist(project);
    }

    public Project read(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Empty input");
        return projectRepository.findOne(id);
    }

    public List<Project> readAll() throws Exception {
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty())
            throw new Exception("Empty storage");
        return projectRepository.findAll();
    }

    public void update(String id, String name, String description, Date start, Date finish) {
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;

        projectRepository.update(id, name, description, start, finish);
    }

    public boolean remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.removeAllByProjectID(id);
        return projectRepository.remove(id);
    }
}

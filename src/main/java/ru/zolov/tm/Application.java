package ru.zolov.tm;

import ru.zolov.tm.command.*;
import ru.zolov.tm.loader.Bootstrap;


public class Application {

    public static final Class<?>[] classes = {HelpCommand.class, ProjectCreateCommand.class,
            ProjectDisplayCommand.class, ProjectEditCommand.class,
            ProjectRemoveCommand.class, TaskCreateCommand.class,
            TaskDisplayCommand.class, TaskEditCommand.class, TaskRemoveCommand.class};

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
    }
}


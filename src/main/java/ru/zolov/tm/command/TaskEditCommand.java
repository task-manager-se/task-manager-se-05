package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Task;

public class TaskEditCommand extends AbstractCommand {
    private final String name = "edit_task";
    private final String description = "Edit task";

    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        for (Task task : bootstrap.getTaskService().readAll()) {
            System.out.println(task);
        }
        System.out.print("Enter task id: ");
        String id = bootstrap.getTerminalService().nextLine();
        System.out.print("Enter new description: ");
        String description = bootstrap.getTerminalService().nextLine();
        bootstrap.getTaskService().update(id, description);
    }
}

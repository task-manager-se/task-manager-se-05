package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Project;

public class ProjectDisplayCommand extends AbstractCommand {
    private final String name = "display_projects";
    private final String description = "Display project list";
    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        for (Project project : bootstrap.getProjectService().readAll()) {
            System.out.println(project);
        }
    }
}

package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Project;

public class ProjectRemoveCommand extends AbstractCommand {
    private final String name = "remove_projects";
    private final String description = "Remove project";
    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        for (Project project : bootstrap.getProjectService().readAll()) {
            System.out.println(project);
        }
        System.out.print("Enter project id: ");
        String id = bootstrap.getTerminalService().nextLine();
        System.out.println(bootstrap.getProjectService().remove(id));
    }
}

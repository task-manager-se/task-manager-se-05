package ru.zolov.tm.command;

public class HelpCommand extends AbstractCommand {
    private final String name = "help";
    private final String description = "Show all commands";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() {
        for (AbstractCommand command : serviceLocator.getCommandList()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}

package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;

public class TaskCreateCommand extends AbstractCommand {
    private final String name = "create_task";
    private final String description = "Create new project";

    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTaskService().readAll();
        System.out.print("Enter project id: ");
        String projectId = bootstrap.getTerminalService().nextLine();
        bootstrap.getTaskService().create(projectId);
        bootstrap.getTaskService().readTaskByProjectId(projectId);
    }
}

package ru.zolov.tm.command;

import java.util.Date;


public class ProjectCreateCommand extends AbstractCommand {
    private final String name = "create_project";
    private final String description = "Create new project";


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter project name:");
        String projectName = serviceLocator.getTerminalService().nextLine();

        System.out.print("Enter description: ");
        String projectDescription = serviceLocator.getTerminalService().nextLine();

        serviceLocator.getProjectService().create(projectName, projectDescription, new Date(), new Date());
    }
}

package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;

import java.util.Date;

public class ProjectEditCommand extends AbstractCommand {
    private final String name = "edit_project";
    private final String description = "Edit project";
    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter project id: ");
        String id = bootstrap.getTerminalService().nextLine();

        System.out.print("Enter new description: ");
        String description = bootstrap.getTerminalService().nextLine();
        bootstrap.getProjectService().update(id, name, description, new Date(), new Date());
    }
}

package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Task;

public class TaskDisplayCommand extends AbstractCommand {
    private final String name = "display_tasks";
    private final String description = "Display task list";

    protected ServiceLocator bootstrap;

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.bootstrap = serviceLocator;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter project id: ");
        String id = bootstrap.getTerminalService().nextLine();
        for (Task task : bootstrap.getTaskService().readTaskByProjectId(id)) {
            System.out.println(task);
        }
    }
}

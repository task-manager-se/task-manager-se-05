package ru.zolov.tm.api;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;

import java.util.List;

public interface ServiceLocator {
    ProjectService getProjectService();

    TaskService getTaskService();

    TerminalService getTerminalService();

    List<AbstractCommand> getCommandList();

}

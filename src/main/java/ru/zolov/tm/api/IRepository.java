package ru.zolov.tm.api;

import java.util.List;

public interface IRepository<E> {

    void create(String name);

    List<E> findAll();

    E findOne(String id);

    void merge(String id, E entity);

    void remove(String id);

    void removeAll();
}
